#!/usr/bin/env python3

# ------------------------------------------------------------------
# assignments/assignment5.py
# Fares Fraij
# ------------------------------------------------------------------

#-----------
# imports
#-----------

from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
import json

app = Flask(__name__)

# Dictionary of books
books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

@app.route('/book/JSON/')
def bookJSON():
    # <your code>
	return render_template('json.html', response = json.dumps(books))

@app.route('/')
@app.route('/book/')
def showBook():
    # <your code>
    return render_template('showBook.html', books = books)
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    # <your code>
    return render_template('newBook.html')

@app.route('/book/newBook/', methods=['GET','POST'])
def bookSearchNew():
    if request.method == 'POST':
        new_name = request.form['name']
        i = 1
        for oneBook in books:
            if int(oneBook['id']) != i:
                break
            i += 1
        new_book = {'title': new_name, 'id': i}
        books.insert(i - 1, new_book)
        return redirect(url_for('showBook'))
    else:
	    return render_template('showBooks.html')	
	

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    # <your code>
    for oneBook in books:
        if int(oneBook['id']) == book_id:
            singleBook = oneBook
    return render_template('editBook.html', singleBook = singleBook)

@app.route('/book/<int:book_id>/editBook/', methods=['GET','POST'])
def bookSearchEdit(book_id):
    if request.method == 'POST':
        new_name = request.form['name']
        for oneBook in books:
            if int(oneBook['id']) == book_id:
                oneBook['title'] = new_name
        return redirect(url_for('showBook'))
    else:
	    return render_template('showBooks.html')	
	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    # <your code>
    for oneBook in books:
        if int(oneBook['id']) == book_id:
            singleBook = oneBook
    return render_template('deleteBook.html', singleBook = singleBook)

@app.route('/book/<int:book_id>/deleteBook/', methods=['GET','POST'])
def bookSearchDelete(book_id):
    if request.method == 'POST':
        i = 0
        for oneBook in books:
            if int(oneBook['id']) == book_id:
                break
            i += 1
        del books[i]
        return redirect(url_for('showBook'))
    else:
	    return render_template('showBooks.html')	

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)